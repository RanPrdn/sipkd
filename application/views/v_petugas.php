    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet">
  <style type="text/css">
     body{
              background: url('<?php echo base_url().'assets/images/background.png' ?>') no-repeat center center fixed;
              background-size: cover;
              height: 100%;
            }   
  .tengah{
    margin-top: 3%
}
img{
  max-width: 60%
}

.grow { 
transition: all .5s ease-in-out;
}

.grow:hover { 
  -webkit-transform: scale(1.3);
  -ms-transform: scale(1.3);
  transform: scale(1.3);

}

#logout {
    width: 2%;
    content: url('<?php echo base_url().'assets/images/logout.png' ?>');
}
#logout:hover {
    content: url('<?php echo base_url().'assets/images/logouthover.png' ?>');
    width: 2%;
}

.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #2D3E50;
   color: white;
}

label{
  font-weight: bold
}


  </style>
  <body>
    <!--Include menu-->




    <div class="container-fluid">
      <a href="https://bpkad.batam.go.id" target="_blank"><img src="<?php echo base_url().'assets/images/logobpkad.png' ?>" style="margin: 20px" class="img-fluid"></a>
     <a href="<?php echo base_url().'Login/logout'?>"><img id="logout" style="width: 2%;float:right;margin: 20px"></a>
     <!-- <center><h1 style="font-family: 'Oswald',sans-serif;margin-bottom: -5%"><b>APLIKASI E-PERBEN</b></h1></center> -->
  </div>
    <div class="tengah">
      <div class="container">
         <form action="">
          <label><b>NAMA SKPD</b></label>
            <!-- <input style="width: 25%" type="text" class="form-control" id="" placeholder="Nama SKPD" name="tanggalterima"> -->
          <select name="kategori" id="kategori" width="25%" class="form-control selectpicker" data-live-search="true" placeholder="Nama SKPD">
            <option value="0">-PILIH-</option>
          <?php foreach($data->result() as $row):?>
                          <option value="<?php echo $row->id_skpd;?>"><?php echo $row->skpd_nama;?></option>
                        <?php endforeach;?>
        </select>
<div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" id="title" placeholder="Title" style="width:500px;">
                  </div>
          <div class="row">
            <div class="col-md-6">
          <div class="form-group">
            <label>Nama Petugas: </label>
            <input type="true" class="form-control" name="nama" value="<?php echo  $this->session->userdata("ses_nama");?>" disabled> 
          </div>

          <div class="form-group">
            <label for="">Tanggal Terima</label>
            <input  type="date" class="form-control" id="date_timepicker_end" placeholder="Tanggal Terima" name="tanggalterima">
          </div>

          <div class="form-group">
            <label for="" class="mb-2 mr-sm-2">Jam Terima</label>
            <input type="text" class="form-control" id="" placeholder="Jam Terima" name="Jam">
          </div>

          <div class="form-group">
                  <label class="control-label">Nomor SPM</label>
                  
                      <select name="subkategori" class="subkategori form-control">
                        <option value="0">-PILIH-</option>
                      </select>                  
              </div>

          <div class="form-group">
            <label for="" class="mb-2 mr-sm-2">Tanggal SPM</label>
            <input type="text" class="form-control" id="" placeholder="Tanggal SPM" name="tanggalspm">
          </div>

          <div class="form-group">
            <label for="" class="mb-2 mr-sm-2">Jenis</label>
            <input type="text" class="form-control" id="" placeholder="Jenis" name="tanggalspm">
          </div>
          <b style="float:left;text-align: center">*Pastikan Kelengkapan Data Pengajuan Sudah Lengkap</b>
        </div>

        <div class="col-md-6">
        <div class="form-group">
            <label>Uraian: </label>
             <input type="text" class="form-control" id="" placeholder="Uraian" name="uraian"> 
          </div>

          <div class="form-group">
            <label for="">Nilai Kotor</label>
            <input type="text" class="form-control" id="" placeholder="Nilai Kotor" name="nilaikotor">
          </div>

          <div class="form-group">
            <label for="" class="mb-2 mr-sm-2">Nilai Potongan</label>
            <input type="text" class="form-control" id="" placeholder="Nilai Potongan" name="nilaipotongan">
          </div>

          <div class="form-group">
            <label for="" class="mb-2 mr-sm-2">ID Billing 1</label>
            <input type="text" class="form-control" id="" placeholder="ID Billing 1" name="Billing1">
          </div>

          <div class="form-group">
            <label for="" class="mb-2 mr-sm-2">ID Billing 2</label>
            <input type="text" class="form-control" id="" placeholder="ID Billing 2" name="Billing2">
          </div>

          <div class="form-group">
            <label for="" class="mb-2 mr-sm-2">ID Billing 3</label>
            <input type="text" class="form-control" id="" placeholder="ID Billing 3" name="Billing3">
          </div>
          <button style="float: right;" type="submit" class="btn btn-danger mt-3">Submit</button>
          </div>
         </form>
      </div>
    </div>
 <!-- /container -->
 <div class="footer"><h3 style="font-family: 'Oswald',sans-serif;float: left;margin-left: 20px;margin-top: 10px">Selamat Datang <?php echo $this->session->userdata('ses_nama');?></h3><h3 style="font-family: 'Oswald',sans-serif;margin-bottom: 1%;float: right;margin-right: 20px;margin-top: 10px"><b>APLIKASI E-PERBENDAHARAAN</b></h3></div>


<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-3.4.1.slim.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-ui-1.12.1/jquery-ui.js'?>"></script>
<!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
 
        <!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/i18n/defaults-*.min.js"></script>
 
   <script type="text/javascript">
  $(document).ready(function(){
    $('#kategori').change(function(){
      var id=$(this).val();
      $.ajax({
        url : "<?php echo base_url();?>/petugas/get_subkategori",
        method : "POST",
        data : {id: id},
        async : false,
            dataType : 'json',
        success: function(data){
          var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option>'+data[i].nomor_spm+'</option>';
                }
                $('.subkategori').html(html);
          
        }
      });
    });
  });
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $( "#title" ).autocomplete({
              source: "<?php echo site_url('petugas/get_autocomplete/?');?>"
            });
        });
    </script>


  </body>
</html>