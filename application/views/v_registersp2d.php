<style type="text/css">
#statistics {
    max-width: 100%;
    content: url('<?php echo base_url().'assets/images/statistics.png' ?>');
}
#statistics:hover {
    content: url('<?php echo base_url().'assets/images/statisticshover.png' ?>');
    max-width: 100%;
}
#realisasi {
    max-width: 100%;
    content: url('<?php echo base_url().'assets/images/realisasi.png' ?>');
}
#realisasi:hover {
    content: url('<?php echo base_url().'assets/images/realisasihover.png' ?>');
    max-width: 100%;
}

#regulasi {
    max-width: 100%;
    content: url('<?php echo base_url().'assets/images/regulasi.png' ?>');
}
#regulasi:hover {
    content: url('<?php echo base_url().'assets/images/regulasihover.png' ?>');
    max-width: 100%;
}

.float{
  position:fixed;
  width:70px;
  height:60px;
  bottom:210px;
  left:40px;
  text-align:center;
}

.float1{
  position:fixed;
  width:60px;
  height:60px;
  bottom:120px;
  left:40px;
  text-align:center;
}
.float2{
  position:fixed;
  width:60px;
  height:60px;
  bottom:40px;
  left:40px;
  text-align:center;
}

</style>
  <body>
    <a href="<?php echo base_url('page') ?>"><img src="<?php echo base_url('assets/images/logonama.png')?>" style="max-width: 100%; margin: 15px"></a>
      <h1 class="animated fadeInLeft faster Gobold" style="font-size: 80px;text-align: center;color: #2D3E50"><b>REGISTER SP2D</b></h1>
      <div class="container" style="margin-top: 50px">
        <div class="row animated zoomInDown delay-0s">
          <div class="col-md-3">
              <div class="card bg-info">
                <div class="card-body">
                  <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D UP</h4>
                  <h1 class="card-text" style="text-align: center">44</h1>
                </div>
              </div>
          </div>

          <div class="col-md-3">
            <div class="card bg-danger">
              <div class="card-body">
                <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D GU</h4>
                <h1 class="card-text" style="text-align: center">6</h1>
              </div>
            </div>
          </div>
          
          <div class="col-md-3">
            <div class="card bg-success">
              <div class="card-body">
                <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D TU</h4>
                <h1 class="card-text" style="text-align: center">2</h1>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="card bg-warning">
              <div class="card-body">
                <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D LS-BJ</h4>
                <h1 class="card-text" style="text-align: center">7</h1>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!--ROW 2-->

      <div class="container pt3" style="margin-top: 20px;">
        <div class="row animated zoomInUp delay-1s">
          <div class="col-md-4">
              <div class="card bg-secondary">
                <div class="card-body">
                  <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D HIBAD DAN BANSOS</h4>
                  <h1 class="card-text" style="text-align: center">5</h1>
                </div>
              </div>
          </div>

          <div class="col-md-4">
            <div class="card bg-warning">
              <div class="card-body">
                <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D LS-GT</h4>
                <h1 class="card-text" style="text-align: center">100</h1>
              </div>
            </div>
          </div>
          
          <div class="col-md-4">
            <div class="card bg-info">
              <div class="card-body">
                <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">TOTAL SP2D</h4>
                <h1 class="card-text" style="text-align: center">164</h1>
              </div>
            </div>
          </div>
        </div>

        <div class="row animated zoomInUp delay-2s" style="margin-top: 20px">
          <div class="col-md-4">
              <div class="card bg-light">
                <div class="card-body">
                  <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D TERBIT</h4>
                  <h1 class="card-text" style="text-align: center">170</h1>
                </div>
              </div>
          </div>

          <div class="col-md-4">
            <div class="card bg-success">
              <div class="card-body">
                <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D MASUK DP</h4>
                <h1 class="card-text" style="text-align: center">125</h1>
              </div>
            </div>
          </div>
          
          <div class="col-md-4">
            <div class="card bg-danger">
              <div class="card-body">
                <h4 class="card-title" style="text-align: center;background-color: #2D3E50;color: white">SP2D CAIR</h4>
                <h1 class="card-text" style="text-align: center">39</h1>
              </div>
            </div>
          </div>
        </div>
      </div>

   
<a href="#" class="float animated fadeIn fast"><img id="statistics"></a>
<a class="float1 animated fadeIn fast" href="<?php echo base_url('realisasi') ?>"><img id="realisasi"> </a> 
<a class="float2 animated fadeIn fas" href="<?php echo base_url('regulasi') ?>"><img id="regulasi"> </a>

 <!--JavaScript-->   
    <script src="<?php echo base_url().'assets/js/jquery-3.2.1.slim.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>

  </body>
</html>
