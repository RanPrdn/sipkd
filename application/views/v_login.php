<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url().'assets/images/iconpemko.ico'?>" type="image/x-icon" />
    <!-- Bootstrap -->
 
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.min.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/animate.css'?>">

    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">

    <style type="text/css">
      body{
        background: url('<?php echo base_url().'assets/images/background.png' ?>') no-repeat center center fixed;
        background-size: cover;
        height: 100%;
        overflow: hidden;
      }

      .logo{
          max-width: 50%;
          padding-top: 70%; 

      }

    </style>
  </head>
  <body class="animated zoomInDown">
    <div class="container">
      <div class="row">
      <div class="col-md-4"></div>
        <div class="col-md-4">
          <form class="form-signin" action="<?php echo base_url().'Login/auth'?>" method="post">
             <center><img src="<?php echo base_url().'assets/images/logo.png' ?>" class="img-fluid logo"></center>
          <h2 class="form-signin-heading" style="font-family: 'Oswald', sans-serif;color: #db5c5c;font-size: 40px;text-align: center">PERBENDAHARAAN</h2>
            <label for="username" class="sr-only">Username</label>
            <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus style="margin-bottom: 5px">
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
          
            <button id="myBtn" class="btn btn-lg btn-dark btn-block" type="submit" style="margin-top: 5px"><b style="color: white">LOGIN</b></button>
            <center><p class="animated shake" style="font-size:20px;font-family: 'Oswald',sans-serif;color: red"><?php echo $this->session->flashdata('msg');?></p></center>
          </form>
        </div>
        <div class="col-md-4"></div>
      </div>
        </div> <!-- /container -->

<!-- <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?> -->

    
    <script src="<?php echo base_url().'assets/js/jquery-3.2.1.slim.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>

  </body>
</html>
