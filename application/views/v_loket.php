  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">
  .tengah{
    margin-top: 10%
}
img{
  max-width: 60%
}

.grow { 
transition: all .5s ease-in-out;
}

.grow:hover { 
  -webkit-transform: scale(1.3);
  -ms-transform: scale(1.3);
  transform: scale(1.3);

}

#logout {
    width: 2%;
    content: url('<?php echo base_url().'assets/images/logout.png' ?>');
}
#logout:hover {
    content: url('<?php echo base_url().'assets/images/logouthover.png' ?>');
    width: 2%;
}

.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #2D3E50;
   color: white;
}
  </style>
  <body>
    <!--Include menu-->




    <div class="container-fluid">
      <a href="https://bpkad.batam.go.id" target="_blank"><img src="<?php echo base_url().'assets/images/logobpkad.png' ?>" style="margin: 20px" class="img-fluid"></a>
     <a href="<?php echo base_url().'Login/logout'?>"><img id="logout" style="width: 2%;float:right;margin: 20px"></a>
     <!-- <center><h1 style="font-family: 'Oswald',sans-serif;margin-bottom: -5%"><b>APLIKASI E-PERBEN</b></h1></center> -->
  </div>
    <center class="tengah">
      <div class="container row">
         
          <div class="col-md-3 animated fadeIn faster grow" style="padding-top: 10px">
           <center><a href="petugas" class="btn btn-lg btn-outline-light text-dark" role="button"> <i style="font-size: 150px" class="fa fa-file-text-o" aria-hidden="true"></i><br><span style="font-family: 'Oswald',sans-serif;">PETUGAS COUNTER</span></a></center>
         </div>
         <div class="col-md-3 animated fadeIn fast grow" style="padding-top: 10px">
          <center><a class="btn btn-lg btn-outline-light text-dark" role="button"> <i style="font-size: 150px" class="fa fa-id-badge" aria-hidden="true"></i><br><span style="font-family: 'Oswald',sans-serif;">KOORDINATOR</span></a></center>
         </div>
         <div class="col-md-3 animated fadeIn slow grow" style="padding-top: 10px">
          <center><a class="btn btn-lg btn-outline-light text-dark" role="button"> <i style="font-size: 150px" class="fa fa-print" aria-hidden="true"></i><br><span style="font-family: 'Oswald',sans-serif;">PEMERIKSA/ CETAK SP2D</span></a></center>
         </div>
          <div class="col-md-3 animated fadeIn slow grow" style="padding-top: 10px">
          <center><a class="btn btn-lg btn-outline-light text-dark" role="button"> <i style="font-size: 150px" class="fa fa-clipboard" aria-hidden="true"></i><br><span style="font-family: 'Oswald',sans-serif;">REKAP</span></a></center>
         </div> 
        </div>
      </center>
 <!-- /container -->
 <div class="footer"><h3 style="font-family: 'Oswald',sans-serif;float: left;margin-left: 20px;margin-top: 10px">Selamat Datang <?php echo $this->session->userdata('ses_nama');?></h3><h3 style="font-family: 'Oswald',sans-serif;margin-bottom: 1%;float: right;margin-right: 20px;margin-top: 10px"><b>APLIKASI E-PERBENDAHARAAN</b></h3></div>
 
  </body>
</html>