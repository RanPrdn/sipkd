<!DOCTYPE html>
<html>
  <head>
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url().'assets/images/iconpemko.ico'?>" type="image/x-icon" />
    <!-- Bootstrap -->
    <!-- Bootstrap -->

    <link href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/animate.css'?>">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
    <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
      
    <style type="text/css">
        body{
              background: url('<?php echo base_url().'assets/images/background.png' ?>') no-repeat center center fixed;
              background-size: cover;
              height: 100%;
            }   

        @font-face 
            {
              font-family: "Gobold";
              src: url('assets/font/Gobold.otf');
            }
 
           .Gobold {
                 font-family: "Gobold";
                 }
    </style>



  </head>