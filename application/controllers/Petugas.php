<?php
class Petugas extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('m_kategori');
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
  }

  function index(){
    $x['data']=$this->m_kategori->get_kategori();
    $this->load->view('v_petugas',$x);
  }
  function get_subkategori(){
    $id=$this->input->post('id');
    $data=$this->m_kategori->get_subkategori($id);
    echo json_encode($data);
  }

  function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->m_kategori->search_blog($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->blog_title;
                echo json_encode($arr_result);
            }
        }
    }
}
